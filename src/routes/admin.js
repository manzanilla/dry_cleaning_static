const compose = require('koa-compose')
const upload = require('@koa/multer')()
const router = require('koa-router')({
  prefix: '/admin',
  sensitive: true
})
const jwtVerifyMiddleware = require('../middlewares/jwtVerify.middleware')
const isAdminMiddleware = require('../middlewares/isAdmin.middleware')
const accessMiddlewares = compose([jwtVerifyMiddleware, isAdminMiddleware])
const adminController = require('../controllers/admin.controller')

router.get('/ping', accessMiddlewares, adminController.ping)

router.post(
  '/uploadFiles',
  accessMiddlewares,
  upload.array('files', 10),
  adminController.uploadFiles
)

module.exports = router
