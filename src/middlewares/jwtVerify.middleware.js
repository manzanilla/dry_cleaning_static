const jwt = require('jsonwebtoken')

module.exports = async (ctx, next) => {
  try {
    const { ACCESS_TOKEN_SECRET } = ctx.env
    const token = ctx.headers.authorization.split(' ')[1]

    try {
      ctx.request.jwtPayload = jwt.verify(token, ACCESS_TOKEN_SECRET)
    } catch (e) {
      console.error(e)

      ctx.status = 401

      return
    }
  } catch (e) {
    ctx.status = 401

    return
  }

  await next()
}
