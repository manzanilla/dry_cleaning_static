module.exports = async (ctx, next) => {
  try {
    const { accountRole } = ctx.request.jwtPayload

    if(accountRole !== 27) {
      ctx.status = 403

      return
    }
  } catch (err) {
    ctx.status = 403
    
    return
  }

  await next()
}
