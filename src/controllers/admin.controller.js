const fs = require('fs')
const sharp = require('sharp')
const { resolve } = require('path')
const shortid = require('shortid')
const ping = (ctx) => {
  ctx.body = 'pong admin'
}

const uploadFiles = async (ctx) => {
  try {
    let { files = [] } = ctx.request

    files = files.filter((el) => {
      const { mimetype } = el
      const mimetypes = ['image/png', 'image/jpeg', 'image/webp']

      return mimetypes.indexOf(mimetype) > -1
    }).map((el) => {
      const { buffer, mimetype, size } = el
      const ext = mimetype.split('/')[1]
      const fileId = shortid.generate()
      const targetDir = resolve(
        __dirname,
        '..',
        '..',
        `files/${fileId.slice(-1)}/${fileId.slice(-2)}`
      )

      fs.mkdirSync(targetDir, { recursive: true })

      const originPath = `${targetDir}/${fileId}.${ext}`
      const avatarPath = `${targetDir}/${fileId}.120x120.${ext}`

      sharp(buffer).resize(120).toFile(avatarPath)

      const url = `${process.env.HOST}/${fileId.slice(-1)}/${fileId.slice(-2)}/${fileId}.${ext}`

      fs.writeFileSync(originPath, buffer)

      return { mimetype, url, size }
    })

    ctx.body = files
  } catch (e) {
    onCatch(ctx, e)
  }
}

const onCatch = (ctx, e) => {
  console.error(e)

  ctx.status = 400
  ctx.body = { message: e.toString() }
}

module.exports = {
  ping,
  uploadFiles
}
