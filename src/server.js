const Koa = require('koa')
const cors = require('@koa/cors')
const app = new Koa()
const serve = require('koa-static')
const { resolve } = require('path')

app.use(cors())
app.use(serve(resolve(__dirname, '..', 'files')))

require('dotenv').config()

app.context.env = process.env

app.use(require('./routes/admin').routes())

app.listen(process.env.API_PORT, () => {
  const message = `Server listening on http://127.0.0.1:${process.env.API_PORT}`

  console.log(message)
})
